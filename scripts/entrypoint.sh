#!/bin/sh
#
# fix ISSUE: os::commit_memory failed; error=Operation not permitted
# (AUFS does not support xattr, so we need to set the flag once again after execution of the container in its entrypoint)
# https://en.wikibooks.org/wiki/Grsecurity/Application-specific_Settings#Java
#

if [ ! -e /usr/bin/setfattr ]; then
	apt-get update -q
	apt-get install -qy attr
fi

setfattr -n user.pax.flags -v em /usr/lib/jvm/*/bin/java /usr/lib/jvm/*/jre/bin/java

exec /docker-entrypoint.sh ${@}
